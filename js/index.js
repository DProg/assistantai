const myForm = document.getElementById("InpForm");
const textbox = document.querySelector(".output");
const monika = document.querySelector(".Monika");
const modal = document.getElementById("modal");
const button = document.getElementById("modalButton");
const closeBtn = document.getElementsByClassName("close")[0];

// При нажатии на кнопку открываем модальное окно
button.onclick = function() {
  modal.style.animation = "slideIn 0.5s forwards";
  modal.style.display = "block";
}

// Закрываем модальное окно при нажатии на кнопку "Закрыть"
closeBtn.onclick = function() {
  modal.style.animation = "slideOut 0.5s forwards";

  // Очищаем анимацию и скрываем окно
  setTimeout(function() {
    modal.style.animation = "";
    modal.style.display = "none";
  }, 500);
}


myForm.addEventListener("submit", async (e)=>{

	e.preventDefault();
    // данные для отправки
    const inpt = document.querySelector(".inputin");
 
    const response = await fetch("/", { 
              method: "POST", 
              headers: { "Content-Type": "application/json" },
              body: JSON.stringify({message: inpt.value})
    });
    // из объекта ответа извлекаем текст ответа

    const data = await response.json();
	textbox.textContent = data.text.text;
    console.log(data.text);

});
