// подключения
const express = require("express");
const CharacterAI = require("node_characterai");
const bodyParser = require("body-parser");
const fs = require("fs");
require('dotenv').config()

// создаем объект приложения
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

const characterAI = new CharacterAI();
characterAI.puppeteerPath = "/usr/bin/chromium";

// change to if you dont have token characterAI.authenticateAsGuest();
characterAI.authenticateWithToken(process.env.TOKEN);

// Обработка высера c сайтa через JSON
const jsonParser = express.json();

app.post("/", jsonParser, function (request, response) {
    const user = request.body;
    console.log(user);
	let data;
	
	(async () => {
		
		// Place your character's id here
		const characterId = "EdSSlsl49k3wnwvMvK4eCh4yOFBaGTMJ7Q9CxtG2DiU";

		// Create a chat object to interact with the conversation
		const chat = await characterAI.createOrContinueChat(characterId);

		data = `You: ${user.message}\n\n`;

		fs.writeFile("log.txt", data, {flag:"a"}, function(error){ 
			if(error){
				return console.log(error);
			}
		});

		// Send a message
		const responsechat = await chat.sendAndAwaitResponse(user.message, true);

		data = `Monika: ${responsechat.text}\n\n`;

		fs.writeFile("log.txt", data, {flag:"a"}, function(error){ 
			if(error){
				return console.log(error);
			}
		});
		
		console.log(responsechat);

		// Use `response.text` to use it as a string
		response.json({text: responsechat}); // отправляем данные на сайт
	
	})();

});


// определяем обработчик для маршрута "/"
app.get("/", function(request, response){

    app.use(express.static("js"));
	app.use(express.static("style"));
	// отправляем ответ
    response.sendFile(__dirname + "/html/index.html");

});

// начинаем прослушивать подключения на 8080 порту
app.listen(8080);


console.log("\nSERVER RUN AT https://127.0.0.1:8080/\n");
