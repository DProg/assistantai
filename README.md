# AssistantAI



## Requirements 

	nodejs
    chromium

## Install

	npm install

## Usage

When using the package, you can:
* Login as guest using `authenticateAsGuest()` - *for mass usage or testing purposes*
* Login with your account or a token using `authenticateWithToken()` - *for full features and unlimited messaging*


### On PC:
1. Open the Character.AI website in your browser (https://beta.character.ai)
2. Open the developer tools (<kbd>F12</kbd>, <kbd>Ctrl+Shift+I</kbd>, or <kbd>Cmd+J</kbd>)
3. Go to the `Application` tab
4. Go to the `Storage` section and click on `Local Storage`
5. Look for the `char_token` key
6. Open the object, right click on value and copy your session token
7. Edit `.env` in root folder of project and put there your token
8. Run `node index.js`

![Session_Token](https://github.com/realcoloride/node_characterai/assets/108619637/1d46db04-0744-42d2-a6d7-35152b967a82)

---
### ⚠️ WARNING: DO NOT share your session token to anyone you do not trust or if you do not know what you're doing. 
#### _Anyone with your session token could have access to your account without your consent. Do this at your own risk._
---

